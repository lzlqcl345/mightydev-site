﻿import React, { Component } from 'react';
import { Route } from 'react-router';

import { Layout } from './components/Layout.jsx';
import { Home } from './components/Home.jsx';
import { About } from './components/About.jsx';
import { Experience } from './components/Experience.jsx';
import { Projects } from './components/Projects.jsx';

export default class App extends Component {
    displayName = App.name

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route exact path='/about' component={About} />
                <Route exact path='/experience' component={Experience} />
                <Route exact path='/projects' component={Projects} />
            </Layout>
        );
    }
}