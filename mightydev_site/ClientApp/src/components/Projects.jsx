﻿import React, { Component } from 'react';

import { LinkContainer } from 'react-router-bootstrap';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

library.add(faAngleLeft);

export class Projects extends Component {
    displayName = Projects.name

    render() {
        return (
            <div>
                <LinkContainer to={'/'}>
                    <button className="btn btn-mighty back"><FontAwesomeIcon icon="angle-left" /> На главную</button>
                </LinkContainer>
                <h2 className="page-title">Проекты</h2>

                <p className="subtitle">1. Сайт mightydev.ru</p>
                <p>Cайт-визитка, написан с использованием библиотеки React поверх ASP.NET Core.</p>
                <p>Ссылка: вы уже здесь находитесь</p>
                <p>Исходный код: <a href="https://gitlab.com/lzlqcl345/mightydev-site" target="_blank" rel="noopener noreferrer">https://gitlab.com/lzlqcl345/mightydev-site</a></p>

                <p className="subtitle">2. Блог blog.mightydev.ru</p>
                <p>Блог, который я изначально написал на ASP.NET MVC, затем переписал на ASP.NET Core. Блог следует принципам минимализма (как и все остальные мои проекты) и 
                    обладает всем необходимым функционалом: авторизацией и аутентификацией, возможностью создавать и редактировать посты с помощью удобного wysiwyg-редактора, 
                    и прочим тривиальным для блога функционалом, как то поиск, фильтрация по категориям, комментирование постов через Disqus и т.д.</p>
                <p>Ссылка: <a href="http://blog.mightydev.ru" target="_blank" rel="noopener noreferrer">http://blog.mightydev.ru</a></p>
                <p>Исходный код: <a href='https://gitlab.com/lzlqcl345/blog' target="_blank" rel="noopener noreferrer">https://gitlab.com/lzlqcl345/blog</a></p>

                <p className="subtitle">3. Хранилище storage.mightydev.ru</p>
                <p>Когда я закончил разрабатывать минимально жинеспособную версию блога, у меня возникла необходимость где-то хранить фото и картинки (плюс место в БД простаивает). 
                    Поэтому за несколько вечеров я написал простое хранилище (веб-приложение на ASP.NET MVC), которое позволяет загружать файлы и возвращает ссылки на них. 
                    Хранилище записывает их в сыром виде в БД и хранит их MIME-типы, поэтому данное хранилище универсально и может быть использовано не только для фотографий.</p>
                <p>Ссылка: <a href='http://storage.mightydev.ru' target="_blank" rel="noopener noreferrer">http://storage.mightydev.ru</a></p>
                <p>Исходный код: <a href='https://gitlab.com/lzlqcl345/storage' target="_blank" rel="noopener noreferrer">https://gitlab.com/lzlqcl345/storage</a></p>

                <p className="subtitle">4. Simple Shutdown</p>
                <p>Я люблю перед сном немного послушать аудио-книги или лекции. Была перспектива постоянно писать в консоли shutdown -s -t 1800, чтобы выключился компьютер 
                    через полчаса, либо пользоваться сторонним софтом с очень избыточным функционалом. Я написал SimpleShutdown на WPF и довольно давно.
                    Это простая десктопная программа, принудительно и безопасно для операционной системы выключает компьютер либо по таймеру с обратным отчетом,
                    либо в указанное пользователем время.</p>
                <p>Скриншот окна программы: <a href='http://storage.mightydev.ru' target="_blank" rel="noopener noreferrer">http://storage.mightydev.ru</a></p>
                <p>Исходный код: <a href='https://gitlab.com/lzlqcl345/storage' target="_blank" rel="noopener noreferrer">https://gitlab.com/lzlqcl345/storage</a></p>
            </div>
        );
    }
}