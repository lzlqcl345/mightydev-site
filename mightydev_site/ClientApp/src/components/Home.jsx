﻿import React, { Component } from 'react'

import { LinkContainer } from 'react-router-bootstrap';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faGitlab, faLinkedinIn } from '@fortawesome/free-brands-svg-icons';

library.add(faEnvelope, faGitlab, faLinkedinIn);

export class Home extends Component {
    displayName = Home.name

    render() {
        return (
            <div className="row home">
                <div className="col-md-6">
                    <img className="img-responsive" src="http://storage.mightydev.ru/api/file/5f0cf461-6887-4a6a-b843-082ef597ff33" alt="mightydev.ru" />
                </div>
                <div className="col-md-6 text-center">
                    <div className="home-greeting">
                        <h3>ПРИВЕТ!</h3>
                    </div>
                    <p className="home-description">Меня зовут Андрей Созонтов, я .NET разработчик и мой опыт работы более 2-х лет. Это мой сайт-CV.</p>
                    <div className="btn-group-vertical">
                        <LinkContainer to={'/about'}>
                            <button className="btn btn-mighty">Обо мне</button>
                        </LinkContainer>
                        <LinkContainer to={'/experience'}>
                            <button className="btn btn-mighty">Опыт работы</button>
                        </LinkContainer>
                        <LinkContainer to={'/projects'}>
                            <button className="btn btn-mighty">Проекты</button>
                        </LinkContainer>
                    </div>
                    <div className="social-buttons">
                        <a href="mailto:lzlqcl345@gmail.com" title="Email"><FontAwesomeIcon icon='envelope' /></a>
                        <a href="https://gitlab.com/lzlqcl345" title="GitLab" target="_blank" rel="noopener noreferrer"><FontAwesomeIcon icon={['fab', 'gitlab']} /></a>
                        <a href="https://www.linkedin.com/in/andrei-sozontov" title="LinkedIn" target="_blank" rel="noopener noreferrer"><FontAwesomeIcon icon={['fab', 'linkedin-in']} /></a>
                    </div>
                </div>
            </div>
        );
    }
}