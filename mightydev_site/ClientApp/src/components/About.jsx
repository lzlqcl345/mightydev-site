﻿import React, { Component } from 'react';

import { LinkContainer } from 'react-router-bootstrap';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons';

library.add(faAngleLeft);

export class About extends Component {
    displayName = About.name

    render() {
        return (
            <div>
                <LinkContainer to={'/'}>
                    <button className="btn btn-mighty back"><FontAwesomeIcon icon='angle-left' /> На главную</button>
                </LinkContainer>
                <h2 className="page-title">Обо мне</h2>
                <p>В данный момент работаю над тем что бы улучшить свои навыки во frontend разработке. Так же интересуюсь архитектурой enterprise-приложений, эффективностью алгоритмов и структур данных, тестированием ПО, построением IT инфраструктуры.</p>
                <p className="subtitle">Основной язык:</p>
                <p>C#</p>
                <p className="subtitle">Языки и инструменты, которыми владею:</p>
                <ul>
                    <li>ASP.NET MVC и ASP.NET Core</li>
                    <li>WCF и WebApi</li>
                    <li>MSSQL и ORM: EF, BLToolKit, linq2db, Dapper</li>
                    <li>HTML, CSS, JS</li>
                </ul>
            </div>
        );
    }
}